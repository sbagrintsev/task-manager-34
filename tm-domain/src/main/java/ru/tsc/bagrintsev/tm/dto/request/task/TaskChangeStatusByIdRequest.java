package ru.tsc.bagrintsev.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    @Nullable
    private String statusValue;

    public TaskChangeStatusByIdRequest(@Nullable String token) {
        super(token);
    }

}
