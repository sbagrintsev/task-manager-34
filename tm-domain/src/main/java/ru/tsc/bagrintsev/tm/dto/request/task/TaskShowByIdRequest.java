package ru.tsc.bagrintsev.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskShowByIdRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    public TaskShowByIdRequest(@Nullable String token) {
        super(token);
    }

}
