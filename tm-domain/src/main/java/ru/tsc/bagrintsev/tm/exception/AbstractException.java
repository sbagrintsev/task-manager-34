package ru.tsc.bagrintsev.tm.exception;

import org.jetbrains.annotations.Nullable;

public abstract class AbstractException extends Exception {

    public AbstractException() {
        super();
    }

    public AbstractException(@Nullable final String message) {
        super(message);
    }

    public AbstractException(
            @Nullable final String message,
            @Nullable final Throwable cause
    ) {
        super(message, cause);
    }

    public AbstractException(@Nullable final Throwable cause) {
        super(cause);
    }

    protected AbstractException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
