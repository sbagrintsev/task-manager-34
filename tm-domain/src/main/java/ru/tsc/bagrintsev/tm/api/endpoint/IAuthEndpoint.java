package ru.tsc.bagrintsev.tm.api.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import jakarta.xml.ws.Service;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignInRequest;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignOutRequest;
import ru.tsc.bagrintsev.tm.dto.request.user.UserViewProfileRequest;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignInResponse;
import ru.tsc.bagrintsev.tm.dto.response.user.UserSignOutResponse;
import ru.tsc.bagrintsev.tm.dto.response.user.UserViewProfileResponse;

import javax.xml.namespace.QName;
import java.net.URL;

@WebService
public interface IAuthEndpoint extends IAbstractEndpoint {

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        return IAbstractEndpoint.newInstance(host, port, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserSignInResponse signIn(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserSignInRequest request
    );

    @NotNull
    @WebMethod
    UserSignOutResponse signOut(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserSignOutRequest request
    );

    @NotNull
    @WebMethod
    UserViewProfileResponse viewProfile(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserViewProfileRequest request
    );

}
