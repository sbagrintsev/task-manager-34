package ru.tsc.bagrintsev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.ICommandService;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.system.ServiceLocatorNotInitializedException;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    public ICommandService getCommandService() throws AbstractException {
        if (serviceLocator == null) throw new ServiceLocatorNotInitializedException();
        return serviceLocator.getCommandService();
    }

    @NotNull
    public IPropertyService getPropertyService() throws AbstractException {
        if (serviceLocator == null) throw new ServiceLocatorNotInitializedException();
        return serviceLocator.getPropertyService();
    }

    @NotNull
    public ISystemEndpoint getSystemEndpoint() throws AbstractException {
        if (serviceLocator == null) throw new ServiceLocatorNotInitializedException();
        return serviceLocator.getSystemEndpoint();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[0];
    }
}
