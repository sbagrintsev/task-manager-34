package ru.tsc.bagrintsev.tm.command.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandsCommand extends AbstractSystemCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        @NotNull Collection<AbstractCommand> repository = getCommandService().getAvailableCommands();
        repository.stream()
                .filter(c -> !c.getName().isEmpty())
                .forEach(c -> System.out.printf("%-35s%s\n", c.getName(), c.getDescription()));
    }

    @NotNull
    @Override
    public String getName() {
        return "commands";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print application interaction commands.";
    }

}
