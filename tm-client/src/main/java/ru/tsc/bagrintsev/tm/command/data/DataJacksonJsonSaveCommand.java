package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJacksonJsonSaveRequest;

public final class DataJacksonJsonSaveCommand extends AbstractDataCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        getDomainEndpoint().saveJacksonJson(new DataJacksonJsonSaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-json";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save current application state in json file";
    }

}
