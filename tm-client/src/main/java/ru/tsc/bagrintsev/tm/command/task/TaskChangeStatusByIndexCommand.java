package ru.tsc.bagrintsev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskChangeStatusByIndexRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.INDEX);
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        showParameterInfo(EntityField.STATUS);
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(getToken());
        request.setIndex(index);
        request.setStatusValue(statusValue);
        getTaskEndpoint().changeTaskStatusByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-change-status-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change task status by index.";
    }

}
