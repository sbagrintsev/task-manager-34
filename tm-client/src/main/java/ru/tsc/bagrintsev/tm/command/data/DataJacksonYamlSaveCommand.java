package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJacksonYamlSaveRequest;

public final class DataJacksonYamlSaveCommand extends AbstractDataCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        getDomainEndpoint().saveJacksonYaml(new DataJacksonYamlSaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-yaml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save current application state in yaml file";
    }

}
